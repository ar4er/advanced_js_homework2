// Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.
// Когда нужно отработать сценарий, при котором с сервера могут прийти не те данные, которые мы ожидаем или вовсе не прийдёт ничего.

const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];

const root = document.getElementById("root");

const list = document.createElement("ul");

root.append(list);

const validKeys = ["author", "name", "price"];

books.map((book, bookIndex) => {
  const listItem = document.createElement("li");
  try {
    for (const key of validKeys) {
      if (!book[key])
        throw (
          "Єлемент масиву books за індексом " +
          bookIndex +
          " не має поля " +
          key +
          "."
        );
      else listItem.innerText += book[key] + ". ";
    }
    list.append(listItem);
  } catch (error) {
    console.log(error);
  }
});
